---
permalink: /blog/
layout: page
title: blog
published: true
---

one day I'll move everything into one platform.

right now, **the main one is [medium](https://medium.cregox.com)**.

meanwhile...

still got those 3 (not 4 anymore, the one from squarespace is no more):

- [oldest](http://cauecmrego.blogspot.com) (blogspot)
- [archived](http://talk.cregox.com/c/blog) (discourse) used to be also a forum.
- [experimental](http://blog.cregox.com) (jekyll) mainly for toying around, I guess. but might become the main one.
