---
layout: page
title: ahoxus - we make wanderful art
published: true
---

[this](https://gitter.im/ahoxus/) is sparta!

ok, not quite...

we actually want to produce [inner voices](https://trello.com/b/2m0jFNqT/inner-voices), a very short music video with a happy tune and fun approach to small things in life that could make a huge difference if [enough people would absorb](https://ncase.me/crowds/) just a little of the concepts brought in it. it'd be our first showcase work.

you can read the [full script here](https://docs.google.com/document/d/1fxKT1NOhUoV2Ziq1q1pwqZC9w6iz9yPJYcSGN5zGThw/edit?usp=sharing).

however, to finish it, we'll need more people and resources. so, right now, the production is paused and we're coming up with an even simpler script as to get the first work done and start moving the wheel!

there's a very [small](https://photos.app.goo.gl/uwfqsslurcNkJLaJ3) video test (just to see if we could make it cutless with zero costs) done for inner voices, which might give you a tiny glimpse of the rithm we want to impose.

in that sense, ahoxus might eventually become a good study case to how anyone can do amazing stuff with absolutely no resources needed (other than what most of us already have in hands). ok, no big secret there, we admit! but we still haven't seen anything like this done before. and we've been around.

now...

why are we writing this clueless manifesto?

<details>
<summary markdown="span">well, this is what i just wrote, in a [genius](https://genius.com/2942509) message, directly to the source...</summary>

> hey there, sia girl!
> 
> i was amazed to see you've came to genius (even if it's not really you). and that, along with reading you've got ziegler after a tweet, got me into thinking i should try it too! so...
> 
> we've got [this music video script](https://docs.google.com/document/d/1zN8fGLmfs5DHWXHbCugltkAPIwjM56wOkQLctEpOmnY/edit?ts=5af1b954) (along with many similar ideas) that i'll probably start pitching as:
> 
> - inner voices is emotional like sia's, but happy and lyric-less. it's also cutless like gambino's, but with no politics ever. perhaps we can add meaningless lyrics, to improve the production value. keep in mind, however, the main take away messages should always be on video, something as culture-independent as possible!
> 
> from the following link you can see everything we and i've done so far (including the script and some very amateur video testing), and go as deeps as you wish into my oddly artistic mind: http://cregox.net/ahoxus
</details>

but when trying to send it, i kept getting a 403 forbidden message. [lol](https://genius.com/discussions/284677-How-can-i-message-an-artist). i gave that up and sent it over twitter instead. at this point, this whole page was already finished, and i wasn't expecting having to reduce the message...

<details>
<summary markdown="span">but it was probably for the best!</summary>

> @Sia hey there, sia girl! check out the music video script in: http://cregox.net/ahoxus and this is the pitch: "inner voices" is emotional like sia’s, but happy and lyric-less. it’s also cutless like gambino’s, but with no politics ever. all as culture-independent as possible!
</details>

as much as we want to be that breakthrough instance of overcoming immense obstacles, we don't mind doing it in other ways. right now it's time to try everything.

do you care?

we do. [soon](ahoxus.org) we'll be able to show just how much.

<small>PS: this is an insider section of ahoxus. there will be no direct link from the site into here... transparency is better offered in many layers.</small>
