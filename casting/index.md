---
permalink: /casting/
layout: page
title: casting
published: true
---

greetings!

i'd love to get casted in any project which would let me do more than acting.

ideally i'd be today getting classes in some sort of group like [ETA](http://www.estudiodetreinamento.com.br/) but focused on videos. they basically teach theater, free of fees (with cheapest rates) and most of their income comes from student plays, starting from the very 1st module. we learn how to get income as an artist. but i still couldn't find such a group here in Lisbon.

despite being a nice actor (proportional to the amount of practice) and generally above average in almost anything i do (sports being one exception) i'm also way too unexperienced in making scenes to get spotted as a professional there. who knows? one day i could get the guts to go live like an artist, "dumped in the streets" (because the absolute majority of artists get no money) and enable myself to start working with some kind of art 24/7... in fact, this may happen in 2018 now.

but i still have little to show as an artist - <small>if you want to start **digging** me up online, however... boy, as a web freak i've created small "artistic" little pieces, such as [this old thing](https://en.wikipedia.org/wiki/User:Cregox/-%3F_wiki%3F) (which is now broken and I don't recall [what was it](https://en.wikipedia.org/w/index.php?title=User:Cregox&oldid=220362058) but it sure was a form of art!). for a terrible instance, [my mom](https://www.quora.com/What-kind-of-hobbies-do-highly-intelligent-i-e-those-with-intelligence-levels-beyond-the-exceptional-people-have/answer/Caue-Rego?srid=ptsW) surely loves every little thing i do over all of her 4 kids. i believe it has to do with "[my artistic side](https://github.com/cauerego/cauerego.github.io/wiki/a-novel-about-the-other-novel)" which me, her and some friends can see.</small>

anyway, below I'm trying to gather a collection of relevant links. hope you can enjoy!

media

- [youtube](https://www.youtube.com/c/cauerego) have some weak videos i've made, might be worth it as a fringe reference.
- [casting](https://b.cregox.com/caue-casting) is a more traditional (and boring) style, if you're into this
- probably i should use more [instagram](https://www.instagram.com/cregox/), haha
- [random fun](/random), [why not](http://talk.cregox.com/t/focus-on-mario-forget-the-rest-of-universe/7919)? randomness kinda says a lot about my current idea-project
- back to [myself](/myself) then, with more actual real photos

somewhat relevant projects

- [Abaixa Tampa (Lower the toilet lid)](http://abaixatampa.wordpress.com/), a theater play in portuguese made in 3 months
- [Funny or Die with Jean Claude](http://www.funnyordie.com/videos/f6f674e14c/just-a-regular-damme-day), “cool” video made in about 5 days, with my worst acting

"poetry" (or some sore of art in form of text)

- [talk.cregox.com](http://talk.cregox.com/) a ["ghost town"](http://talk.cregox.com/t/a-beginning-for-the-forums-here/7#7) I've made and home to most of my [blog](/blog) as an [inform**art**icist](http://talk.cregox.com/t/to-kstanley-can-a-neat-mario-start-up-a-basiux/7914) and [progammer*](http://talk.cregox.com/t/yeah-im-also-a-progammer/7676)
- [The next Ai poca lips](http://blog.cregox.com/the-next-ai-poca-lips-r23K6m6)
