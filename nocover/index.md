---
layout: page
title: no cover
published: true
---

i hate cover letters. and you should too. here's why.

keep in mind, this is no blog post. and i couldn't quickily fine one.
so i'll collect a many links here and keep the text to as short as i can.

# job vacancies are overestimated

it all starts there. the thing is, a job is by far the easiest way to get money. and a steady flow of it.
so jobs are great, if you [can get one](https://medium.freecodecamp.org/ten-rules-for-negotiating-a-job-offer-ee17cccbdab6)!
most of us believe we should grind for it, and there are [great ways](https://haseebq.com/how-to-break-into-tech-job-hunting-and-interviews/) to do it.
but that's just one part of the reality. certainly the biggest one. still, not the only significant one.
especially if we're not looking to work with what's in high demand.

# cover letters are overestimated!

the right way to do a cover letter is called **resume**.
the letter is simply a lazy way to customize your introduction to a company.
that's really what the resume should be all about... if we stop grinding.

then again, this is the hardest path. it's so much easier to go with haseeb's excelent guides, as already mentioned.
yes, we still have to count on luck... but not really.
if we keep on going and insisting, with so much demand we're deemed to find something.
thus, cover letters.

fuck that.

# then what?

portfolio. of one's life. and i've got plenty of it, as instance. just [hit home](/) and google me up, with the customized engine there.

[get to know yourself](https://waitbutwhy.com/2018/04/picking-career.html). [deeply](https://trello.com/b/HmA5nlx6/self-knowledge).

that's a lot of what [ahoxus](/ahoxus) is all about, but that's a spin off topic...

the main take away is to do everything you can, daily, to chase your dreams. regardless of life. because of life.
it is beautiful, after all. [enjoy](/tv).

<small>PS: and do yourself a favor, write your own generic cover letter, inside your own portfolio, to link it everytime you feel the urge to apply to a vacancy for any reason.
it can save us a lot of time!</small>

and cheers! :)
