---
permalink: /contact/
layout: page
title: contact
published: true
menu: true
redirect_from: /contacts/
---

aha, no form here!

you may:

- mail [help@cregox.net](mailto:help@cregox.net).
- reach me online. on **skype** at _**caue.rego**_, hangouts with **caue.rego@gmail.com**, whatsapp, telegram, twitter, facebook, etc.
- **try*** calling me with [google](https://en.wikipedia.org/wiki/GrandCentral) at **+1 732 7377346** (or leave a message there) or here in Portugal at **+351 300506270** (*_telephony and voip services are just not that reliable to connect calls_).
- be creative, surprise me. :)

i usually reply in a few hours. if i don't answer in a couple of days, by all means do nag me again.

![ana boat](caue on ana boat.jpeg)
*I wish I lived in such a boat... Flying in the sky...*
